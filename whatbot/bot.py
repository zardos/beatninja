# -*- coding: utf-8 -*-
import json
import requests
import time
import suds

class MainBot():
    def __init__(self, message):
        self.message = message
        self.help_msg = """Heyho
        I\'m your Bot, please use one of the following commands:
        ping, time, joke, muelldienst, playlist, add <youtube-URL>, buli"""

    def react(self):
        self.message = self.message.upper()
            if "HELP"  in self.message or \
               "HI"    in self.message or \
               "HELLO" in self.message or \
               "HALLO" in self.message:
                return self.help_msg

            elif "PING" in self.message:
                return "pong"

            elif "TIME" in self.message:
                return getTime()

            elif "PLAYLIST" in self.message:
                return self.sendPlaylist()

            elif "ADD" in self.message:
                return self.addToPlaylist()

            elif "JOKE" in self.message:
                return self.tellMeAJoke()

            elif "MUELLDIENST" in self.message:
                return self.werHatMuelldienst()

            elif "JOSEF?" in self.message or
                 "JOSEF" in self.message:
                return 'sally David! FRU: 04X3787'

            elif "BULI" in self.message:
                return self.bundesliga('bl1', 2015)
            else:
                return 'try harder ^^'

    def timestamp():
        return time.time()


    def bundesliga(self, league, season):
        """
        result_list.append([el[0],   # match_id
                                                el[5],   # spieltag
                                                el[6],   # Spieltag (lesbar)
                                                el[10],  # liga
                                                el[8],   # Liga (lesbar)
                                                el[9],   # saison
                                                #unicode(el[11]), # teamName1
                                                #unicode(el[12]), # teamName2
                                                (el[11].encode('utf-8')), # teamName1
                                                (el[12].encode('utf-8')), # teamName2
                                                el[17],  # tore1
                                                el[18],  # tore2
                                                el[1],   # matchDateTime
                                                el[13],  # teamId1
                                                el[14],   # teamId2
                                                el[21],  # matchResults
                                                el[22],   # goals
                                                el[23],   # Location (Stadion)
                                                el[24],  # Zuschauerzahl
                                                ])

        """

        max_matchdays = 34
        url = "http://www.openligadb.de/Webservices/Sportsdata.asmx?WSDL"
        oldb = suds.client.Client(url)

        for day in range(1, max_matchdays):
            oldb_client = oldb.service.GetMatchdataByGroupLeagueSaison(day, league, season)
            for el in oldb_client[0]:
                if el[17] == -1: # Did the match already happend?
                    continue
                results  = "Spieltag {}\n".format(day)
                results += "{} vs {}".format(el[11], el[12])
                results += " - {}:{} ({}:{})".format(el[21]['matchResult'][1]['pointsTeam1'],
                                                     el[21]['matchResult'][1]['pointsTeam2'],
                                                     el[21]['matchResult'][0]['pointsTeam1'],
                                                     el[21]['matchResult'][0]['pointsTeam2'])
                results += "\n"
            results += "\n\n"
        return results



    def addToPlaylist(self):
        if "https://youtube.com/watch?v=" in self.message:
            playlistFile = open('./flaskmedia/playlist.pls', 'a')
            playlistFile.write(self.message)
            playlistFile.close()
            return 'link added to playlist'
        else:
            return 'only youtubelinks are supported so far...'


    def sendPlaylist(self):
        path = "./flaskmedia/playlist.pls"
        return open(path, 'r').read()


    def werHatMuelldienst(self):
        return "Gewonnen hat Eunice!"


    def tellMeAJoke(self):
        url = "http://api.icndb.com/jokes/random/"
        try:
	    res = requests.get(url)
            joke = json.loads(res)
            return "{}".format(joke['value']['joke'])
        except:
            return("Ouou, think Chuck Norris killed the server...")

