import pafy
from flask import Flask, render_template, url_for, request


class Song(object):
	def __init__(self, url):
		self.url = url
		self.title = ''
		self.artist = ''
		self.duration = ''
		self.stream = ''

		if "youtube" in self.url:
			self.youtube()

	def youtube(self):
		self.__video = pafy.new(self.url)
		self.title = self.__video.title
		self.artist = self.__video.author
		self.duration = self.__video.duration
		self.stream = self.__video.streams[0].url


class Playlist(object):
	def __init__(self):
		self.__playlistFile = 'playlist.pls'
		self.songUrls = self.getUrllist()
		self.playlist = []
		self.newSong = ''

	def getUrllist(self):									#Read URLs from playlistfile
		self.urlfile = open(self.__playlistFile, 'r')		
		self.urls = self.urlfile.read().splitlines()
		self.urlfile.close()
		return self.urls 									#type(self.urls) = list

	def addSong(self, url):									#Appand song to playlist.pls file
		self.urlfile = open(self.__playlistFile, 'a')
		self.urlfile.write(url + '\n')
		self.urlfile.close()
		self.newSong = Song(url)
		#print self.newSong
		self.playlist.append(self.newSong)					#Fill Playlist with Song-Objects

	def createPlaylist(self):
		self.playlist = []									#Clears Playlist for recreation 
		self.songUrls = self.getUrllist()				
		print self.songUrls

		for songUrl in self.songUrls:
			self.playlist.append(Song(songUrl))

		return self.playlist 								#type(self.playlist) = list



app = Flask(__name__)
app.debug = True

@app.route('/')
def index():
	playlist = Playlist().createPlaylist()
	return render_template('index.html', title = 'Playerle', playlist = playlist)
	

@app.route('/addSong', methods=['POST'])
def player():
	newUrl = request.form['url']
	Playlist().addSong(newUrl)
	playlist = Playlist().createPlaylist()
	return render_template('index.html', title = 'Playerle', playlist = playlist)


if __name__ == '__main__':
	app.run(host = '0.0.0.0', debug = True)