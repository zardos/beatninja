#! /usr/bin/python2
from whatsappreceivesend import EchoLayer
from yowsup.layers.auth                        import YowAuthenticationProtocolLayer, YowCryptLayer
from yowsup.layers.protocol_messages           import YowMessagesProtocolLayer
from yowsup.layers.protocol_receipts           import YowReceiptProtocolLayer
from yowsup.layers.protocol_acks               import YowAckProtocolLayer

from yowsup.layers.protocol_media 			   import YowMediaProtocolLayer
from yowsup.layers.protocol_iq				   import YowIqProtocolLayer
from yowsup.layers.protocol_calls              import YowCallsProtocolLayer
from yowsup.layers.axolotl                     import YowAxolotlLayer
from yowsup.layers.logger                      import YowLoggerLayer
from yowsup.layers.stanzaregulator             import YowStanzaRegulator

from yowsup.layers.network                     import YowNetworkLayer
from yowsup.layers.coder                       import YowCoderLayer
from yowsup.stacks import YowStack
from yowsup.common import YowConstants
from yowsup.layers import YowLayerEvent
from yowsup.stacks import YowStack, YOWSUP_CORE_LAYERS
from yowsup import env

import logging
#logging.basicConfig(level=logging.DEBUG)

CREDENTIALS = ("4915144998660", "houVdV/MzdekWLkTDWo8mAmMK9c=") # replace with your phone and password

if __name__==  "__main__":
    #layers = (EchoLayer,(YowAuthenticationProtocolLayer, YowMessagesProtocolLayer, YowReceiptProtocolLayer, YowAckProtocolLayer)) + YOWSUP_CORE_LAYERS
    layers = (EchoLayer, (YowAuthenticationProtocolLayer, YowMessagesProtocolLayer, YowReceiptProtocolLayer, YowAckProtocolLayer, YowMediaProtocolLayer, YowIqProtocolLayer),
        YowLoggerLayer, 
        YowCoderLayer,
        YowCryptLayer,
        YowStanzaRegulator,
        YowNetworkLayer) + YOWSUP_CORE_LAYERS
	

    stack = YowStack(layers)
    stack.setProp(YowAuthenticationProtocolLayer.PROP_CREDENTIALS, CREDENTIALS)         #setting credentials
    stack.setProp(YowNetworkLayer.PROP_ENDPOINT, YowConstants.ENDPOINTS[0])             #whatsapp server address
    stack.setProp(YowCoderLayer.PROP_DOMAIN, YowConstants.DOMAIN)              
    stack.setProp(YowCoderLayer.PROP_RESOURCE, env.CURRENT_ENV.getResource())           #info about us as WhatsApp client

    stack.broadcastEvent(YowLayerEvent(YowNetworkLayer.EVENT_STATE_CONNECT))            #sending the connect signal

    stack.loop() #this is the program mainloop
