#!/usr/bin/env python
"""Whatsapp Party Bot"""
from setuptools import find_packages, setup

setup(name = "beatninja",
        version = "0.1",
        description = "A Whatsapp Bot, which like to party",
        platforms = ["Linux"],
        author = "z4rd0z",
        author_email = "",
        url = "",
        license = "MIT",
        packages = find_packages())
